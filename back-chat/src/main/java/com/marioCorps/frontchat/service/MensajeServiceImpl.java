package com.marioCorps.frontchat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marioCorps.frontchat.document.Mensaje;
import com.marioCorps.frontchat.repository.IMensajesRepository;

@Service
public class MensajeServiceImpl implements IMensajeService {
	
	@Autowired
	IMensajesRepository mensajesRepo;

	@Override
	public List<Mensaje> primeros10Msj() {
		return mensajesRepo.findFirst10ByOrderByFechaDesc();
	}

	@Override
	public Mensaje guardarMensaje(Mensaje msjToSave) {
		return mensajesRepo.save(msjToSave);
	}
}
