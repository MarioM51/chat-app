package com.marioCorps.frontchat.service;

import java.util.List;

import com.marioCorps.frontchat.document.Mensaje;

public interface IMensajeService {	
	List<Mensaje> primeros10Msj();
	Mensaje guardarMensaje(Mensaje msjToSave);  
}
