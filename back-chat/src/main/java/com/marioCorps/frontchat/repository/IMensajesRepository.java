package com.marioCorps.frontchat.repository;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.marioCorps.frontchat.document.Mensaje;
public interface IMensajesRepository extends MongoRepository<Mensaje, String> {
	public List<Mensaje> findFirst10ByOrderByFechaDesc();
}
