package com.marioCorps.frontchat;

import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@org.springframework.context.annotation.Configuration
@org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry
			.addEndpoint("/chat-websocket")  //Url a la que se conectara el usr
			.setAllowedOrigins("http://localhost:4200") //CORS
			// indicamos que usaremos esta libreria, en caso contrario deberiamos usar la lib nativa de HTML5
			// pero este es mas seguro ya que el otro solo soporta el protocolo ws
			.withSockJS(); 
	}

	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		//registramos el prefijo el cual detectara a traves de este los demas canales
		//ejemplo "/chat/msj" por el prefijo de este lo cataloga
		registry.enableSimpleBroker("/chat/");
		
		//Indicamos el prefijo a las rutas a las que se registrara el usuario ej.
		//"app/mensajes" detecta el prefijo 
		registry.setApplicationDestinationPrefixes("/app");
	}
}
