package com.marioCorps.frontchat.controller;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import com.marioCorps.frontchat.document.Mensaje;
import com.marioCorps.frontchat.service.IMensajeService;

@org.springframework.stereotype.Controller
public class ChatController {
	private String colores[] ={ "red", "green", "blue", "yellow", "orange", ""  };
	
	@Autowired
	IMensajeService mensajeService;
	
	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;
	
	@MessageMapping("/mensaje")
	@SendTo("/chat/mensaje")
	public Mensaje recibeMensaje(Mensaje msj) {
		msj.setFecha(new Date().getTime());
		msj.setTexto(msj.getTexto());
		
		if(msj.getTipo().equals("new_user")) {
			msj.setColor(colores[new Random().nextInt(colores.length)]);
			msj.setTexto("Nuevo Usuario");
		} else {
			mensajeService.guardarMensaje(msj);
		}
		
		return msj;
	}
	
	@MessageMapping("/estoyEscribiendo")
	@SendTo("/chat/quienEscribe")
	public String estaEscribiendo(String username) {
		return username.concat(" esta escribiendo ...");
	}
	
	
	@MessageMapping("/historial")
	public void historial(String clienteId) {
		this.simpMessagingTemplate.convertAndSend("/chat/historial/"+clienteId, mensajeService.primeros10Msj());
	}
}
