package com.marioCorps.frontchat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrontChatApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrontChatApplication.class, args);
	}

}
