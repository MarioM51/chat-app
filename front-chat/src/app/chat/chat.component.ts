import { Component, OnInit } from '@angular/core';
import { Client } from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import { PATH_BASE_CHAT, NEW_MESSAGE, NEW_USER } from '../consts';
import { Mensaje } from './models/mensaje';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  private client: Client

  mensaje:Mensaje = new Mensaje();
  mensajes:Mensaje[] = [];

  conectado:boolean = false;
  actualMenteEscribiendo: string;

  clienteId:string;

  constructor() { 
    this.clienteId = "id-" + new Date().getUTCMilliseconds() +"-" +Math.random().toString(36).substr(2);
  }

  ngOnInit() {
    this.client = new Client();
    this.client.webSocketFactory = ()=> {
      return new SockJS(PATH_BASE_CHAT + '/chat-websocket');
    }
    //asignamos el listener de si se conecta
    this.client.onConnect = (frame) => {
      this.conectado = this.client.connected;
      
      //para suscribirnos y escuchar si otro usuario envia mensaje
      this.client.subscribe('/chat/mensaje', (event)=>{ 
        let mensajeRecibido:Mensaje = JSON.parse(event.body) as Mensaje;
        this.mensajes.push(mensajeRecibido);

        if(!this.mensaje.color && mensajeRecibido.tipo==NEW_USER && this.mensaje.username == mensajeRecibido.username) {
          this.mensaje.color = mensajeRecibido.color;
          console.log(!this.mensaje.color, "&&", mensajeRecibido.tipo==NEW_USER, "&&" + this.mensaje.username==mensajeRecibido.username);
          
        }
      });

      //escuchamos por quien mas esta escribiendo
      this.client.subscribe('/chat/quienEscribe', (event)=>{ 
        this.actualMenteEscribiendo = event.body;
        setTimeout(()=>{
          this.actualMenteEscribiendo = '';
        }, 3000)
      });

      //escuhamos si nos llega el historiañ
      this.client.subscribe("/chat/historial/"+this.clienteId, (event)=> {
        const historial = JSON.parse(event.body) as Mensaje[];
        //pasamos fecha UTC a obj-JS y volteamos el arreglo para que se vea bien el orden de los msj
        this.mensajes=historial.map(m=>{m.fecha=new Date(m.fecha);return m;}).reverse();
      });

      //indicamos que queremos que nos llehue el historial
      this.client.publish({ destination: "/app/historial", body: this.clienteId})

      // enviamos notificacion de que se conecto X usuario
      this.mensaje.tipo = NEW_USER;
      this.client.publish({ destination: `/app/mensaje`, body: JSON.stringify(this.mensaje) });
    
    }//end-onConnect

    this.client.onDisconnect = (frame) => {
      this.conectado = this.client.connected;
      
      this.mensaje = new Mensaje();
      this.mensajes = [];
    }//end-onDisconnect
  }

  public btnEnviarMensaje() {
     this.mensaje.tipo = NEW_MESSAGE;
     this.client.publish({destination:`/app/mensaje`,body:JSON.stringify(this.mensaje)});
     this.mensaje.texto = '';
  }


  public eventTextoKeyUp(){
    this.client.publish({destination:`/app/estoyEscribiendo`, body: this.mensaje.username});
  }

  public btnConectar() {
    this.client.activate();
  }

  public btnDesconectar() {
    this.client.deactivate();
  }

}
